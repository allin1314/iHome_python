import redis


class Config(object):
    """配置信息"""
    SCRET_KEY = "DFSAG*5G6DSGSG3DGDSADHHR00"

    # 数据库
    SQLALCHEMY_DATABASE_URI = 'mysql://root:123456@127.0.0.1:3306/iHome_python'
    SQLALCHEMY_TRACK_MODIFICATIONS = True

    # redis
    REDIS_HOST = '127.0.0.1'
    REDIS_PORT = 6379

    # flask-session配置
    SESSION_TYPE = 'redis'
    SESSION_REDIS = redis.StrictRedis(host=REDIS_HOST, port=REDIS_PORT)
    SESSIONI_USE_SIGNER = True  # 将cookie中session_id进行隐藏处理
    PERMANENT_SESSION_LIFETIME = 86400  # session数据的有效期


class DevelopmentConfig(Config):
    """开发模式的配置信息"""
    DEBUG = True


class ProductConfig(Config):
    """生产环境配置信息"""



config_map = {
    'develop': DevelopmentConfig,
    'product': ProductConfig
}
